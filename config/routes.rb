Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get '/', to: 'answer#index'
  get '/star', to: 'answer#star'
  get '/fibonacci', to: 'answer#fibonacci'
  get 'reverse-array', to: 'answer#reverse_array'
  get '/remove-duplicate-array', to: 'answer#remove_duplicate_array'
  get '/print-duplicate-element', to: 'answer#print_duplicate_element'
  get '/largest-smallest-element', to: 'answer#largest_smallest_element'
end
