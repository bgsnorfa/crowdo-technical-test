class AnswerController < ApplicationController
  def index
    render :html => '
      <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
      <html>
      <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <title></title>
        <meta name="generator" content="LibreOffice 6.4.4.2 (Linux)"/>
        <meta name="created" content="00:00:00"/>
        <meta name="changed" content="00:00:00"/>
        <style type="text/css">
          @page { size: 8.27in 11.69in; margin: 0.79in }
          p { margin-bottom: 0.1in; line-height: 115%; background: transparent }
          pre { font-family: "Liberation Mono", monospace; font-size: 10pt; background: transparent }
        </style>
      </head>
      <body lang="en-US" link="#000080" vlink="#800000" dir="ltr"><pre># Answer for Technical Test

      This is the answer for Crowdo Technical Test.
      The codes specifically written at app/answer_controller.rb

      ### List of Answers
      #### Number one
      * <a href="/star?n=10&choice=1">Answer A</a>
      * <a href="/star?n=10&choice=2">Answer B</a>
      * <a href="/star?n=10&choice=3">Answer C</a>
      * <a href="/star?n=10&choice=4">Answer D</a>

      #### Number two
      * <a href="/fibonacci?n=20">Fibonacci</a>

      #### Number three
      * <a href="/reverse-array?array=[1,2,7,6]">Reverse Array</a>

      #### Number four
      * <a href="/remove-duplicate-array?array=[1,2,2,5,4]">Remove duplicate array</a>

      #### Number five
      * <a href="/print-duplicate-element?array=[1,2,2,5,4]">Print duplicate element</a>

      #### Number six
      * <a href="/largest-smallest-element?array=[1,4,100,5,3]">Largest Smallest element</a>

      #### Number seven
      Error will be thrown because syntax `.where("created_at >= ? AND created_at < ?";, date1, date2)` is ambiguous (created_at column are defined at both Article and Comment models).

      #### Number eight
      The codes can be simplified by changing `if/elsif` to `case when` to enhance readibility
      ```
      class ArticlesController < ApplicationController
        def update
          article = Article.find(params[:id])

          article.update(form_params)

          case article.genre
          when "Pop"
            article.publish_date = Date.today + 1.month
            article.status = "pending_review"
          when "Animals"
            article.publish_date = Date.today + 2.weeks
            article.status = "pending_approval"
          end
          article.save!

          if article.status == "pending_review"
            writer = article.writer
            if writer.articles.count > 10
              writer.status = "pending_upgrade_review"
              writer.save!
              SendWriterUpgradeMail.perform_now(writer)
            end
          end
        end
      end
      ```
      </body>
      </html>
    '.html_safe
  end

  def star
    count = params['n'].to_i
    choice = params['choice'].to_i

    if count.nil?
      count = 5
    end

    if choice.nil? or !choice.between?(1, 4)
      choice = 1
    end

    star = ""

    case choice
    when 1
      iter = 0
      count.times do
        iter = iter + 1
        iter.times do
          star += "*"
        end
        star += "<br>"
      end
    when 2
      iter = count
      count.times do
        iter.times do
          star += "*"
        end
        star += "<br>"
        iter = iter - 1
      end
    when 3
      iter = 0
      count.times do 
        iter = iter + 1
        space = count - iter
        p space
        space.times do
          star += "&nbsp;&nbsp;"
        end
        iter.times do
          star += "*"
        end
        star += "<br>"
      end
    when 4
      iter = 0
      count.times do 
        iter = iter + 1
        space = count - iter
        p space
        space.times do
          star += "&nbsp;"
        end
        iter.times do
          star += "*"
        end
        star += "<br>"
      end
    else 
      star = "undefined"
    end
    
    render :html => star.html_safe
  end

  def fibonacci
    n = params['n'].to_i
    n = 0 if n < 0

    arr = []
    n.times do |idx|
      arr.push(calculate_fibonacci idx)
    end

    render :html => arr.to_s.html_safe
  end

  def reverse_array
    array = str_to_arr params['array']

    reversed_arr = []
    (array.length - 1).downto(0).each do |idx|
      reversed_arr.push(array[idx])
    end

    render :html => reversed_arr.to_s.html_safe
  end

  def remove_duplicate_array
    array = str_to_arr params['array']
    if array.any?
      array = array.uniq
    end

    render :html => array.to_s.html_safe
  end

  def print_duplicate_element
    array = str_to_arr params['array']
    duplicated_elem = Hash.new(0)

    array.each do |val|
      duplicated_elem[val] += 1
    end

    duplicated_elem.each do |key, val|
      if val == 1
        duplicated_elem.delete(key)
      end
    end

    render :html => duplicated_elem.to_s.html_safe
  end

  def largest_smallest_element
    array = str_to_arr params['array']
    if array.any?
      array = array.uniq
      array = array.sort_by(&:to_i)
      str = "smallest number = #{array[0]} and largest number = #{array[array.length - 1]}"
    else 
      str = "No array provided"
    end

    render :html => str.html_safe
  end

  private
  
  def calculate_fibonacci(n)
    return n if n <= 1
    calculate_fibonacci(n - 1) + calculate_fibonacci(n - 2)
  end

  def str_to_arr(str)
    unless str.nil?
    str.tr('[]', '').split(',').map(&:to_i)
    else
      str = []
    end
  end

end
