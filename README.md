# README

This is the answer for Crowdo Technical Test.
The codes specifically written at app/answer_controller.rb

### List of Answers
#### Number one
* Answer A : /star?n=10&choice=1
* Answer B : /star?n=10&choice=2
* Answer C : /star?n=10&choice=3
* Answer D : /star?n=10&choice=4

#### Number two
* Go to to /fibonacci?n=10

#### Number three
* Go to /reverse-array?array=[1,2,7,6]

#### Number four
* Go to /remove-duplicate-array?array=[1,2,2,5,4]

#### Number five
* Go to /print-duplicate-element?array=[1,2,2,5,4]

#### Number six
* Go to /largest-smallest-element?array=[1,4,100,5,3]

#### Number seven
Error will be thrown because syntax `.where("created_at >= ? AND created_at < ?", date1, date2)` is ambiguous (created_at column are defined at both Article and Comment models).

#### Number eight
The codes can be simplified by changing `if/elsif` to `case when` to enhance readibility
```
class ArticlesController < ApplicationController
  def update
    article = Article.find(params[:id])

    article.update(form_params)

    case article.genre
    when 'Pop'
      article.publish_date = Date.today + 1.month
      article.status = 'pending_review'
    when 'Animals'
      article.publish_date = Date.today + 2.weeks
      article.status = 'pending_approval'
    end
    article.save!

    if article.status == 'pending_review'
      writer = article.writer
      if writer.articles.count > 10
        writer.status = 'pending_upgrade_review'
        writer.save!
        SendWriterUpgradeMail.perform_now(writer)
      end
    end
  end
end
```

### Configuration
* Clone this repository and run `bundle install`

* Ruby version: 2.6.5

* System dependencies: rails ^6.0.3, puma ^4.1
